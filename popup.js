// function restaurerLesOptions()//resélectionner les options déja choisies
// 		{
// 		document.getElementById('nom').value=localStorage['nom'];//remplissage du champs de nom
// 		var couleur = localStorage['couleur'];//sélection de la liste déroulante de couleur (un peu plus dur)
// 		if (!couleur){
// 			return;
// 			}
// 		var choix = document.getElementById('couleur').getElementsByTagName('option');
// 		for (var i = 0; i < choix.length; i++)
// 			{
// 			if (choix[i].value == couleur)
// 				{
// 				choix[i].selected = "true";
// 				break;
// 				}
// 			}
// 		}

// function enregistrer()	//enregistrer les options, fonction appelée par le click sur le bouton
// {
// 	localStorage['nom']=document.getElementById('nom').value;
// 	localStorage['couleur']=document.getElementById('couleur').value;
// }

window.onload = function() {	

	console.log("popup.js fonctionne");

	var monObjet = {};
	var boutonElt = document.getElementById("bouton");
	boutonElt.addEventListener('click', function() {
		
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		  	chrome.tabs.sendMessage(tabs[0].id, {statut: "start"}, function(response) {
		  		buildTable(response);
		    	
		  	});
		});
	})

	window.buildTable = function(obj) {

		var tableElt = document.createElement("table");
		tableElt.classList.add("table");
		var theadElt = document.createElement("thead");
		var trElt = document.createElement("tr");
		var th1Elt = document.createElement("th");
		th1Elt.textContent = "Nom";
		var th2Elt = document.createElement("th");
		th2Elt.textContent = "Valeur";
		var th3Elt = document.createElement("th");
		th3Elt.textContent = "Action";
		var th4Elt = document.createElement("th");
		th4Elt.textContent = "---";

		trElt.append(th1Elt, th2Elt, th3Elt, th4Elt);
		theadElt.appendChild(trElt);
		tableElt.appendChild(theadElt);

		document.getElementById("variables").appendChild(tableElt);
		
		if (obj) {

			let i = 1;
			var tbodyElt = document.createElement("tbody");

			for (key in obj) {
				var rowElt = document.createElement("tr");
				var headElt = document.createElement("th");
				headElt.textContent = i;

				var data1Elt = document.createElement("td");
				data1Elt.textContent = key;

				var data2Elt = document.createElement("td");
				data2Elt.textContent = obj[key];

				var data3Elt = document.createElement("td");
				data3Elt.textContent = "à définir";

				rowElt.append(data1Elt, data2Elt, data3Elt);
				tbodyElt.appendChild(rowElt);
				tableElt.appendChild(tbodyElt);
			}
		}

	}
}

